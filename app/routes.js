app.config(function($routeProvider) {


        //$routeProvider.when().otherwise();

        $routeProvider
        .when('/', {
                templateUrl : 'app/templates/home.html',
                controller  : 'HomeController'
        })
         .when('/movies/:movieId', {
                templateUrl : 'app/templates/movie.html',
                controller  : 'MovieController'
        })
        .otherwise('/');

});